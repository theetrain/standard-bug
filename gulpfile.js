var gulp = require('gulp')
var standard = require('gulp-standard')

var pathString = './src/**/*.js'
var pathOBJ = {
  path: './src/**/*.js'
}

gulp.task('test', function (cb) {
  return gulp.src([pathOBJ.path])
    .pipe(standard())
    .pipe(standard.reporter('default', {
      breakOnError: true,
      quiet: true
    }))
})
